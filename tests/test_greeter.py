
def test_greeter(Greeter, accounts):
    greeter = accounts[0].deploy(Greeter)

    transaction = greeter.greet()
    assert transaction.return_value.decode('utf-8') == 'Hello'


def test_custom_greeting(Greeter, accounts):
    greeter = accounts[0].deploy(Greeter)

    set_txn_hash = greeter.setGreeting(b'Guten Tag')

    transaction = greeter.greet()
    assert transaction.return_value.decode('utf-8') == 'Guten Tag'
